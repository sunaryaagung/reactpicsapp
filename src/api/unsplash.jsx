import axios from "axios";

export default axios.create({
  baseURL: "https://api.unsplash.com",
  headers: {
    Authorization:
      "Client-ID df64964269edf862db7bd7ea7d17e78993b16f91a715cfd4019b0dceed07c8ae"
  }
});
